# yaohuo

#### 介绍

妖火吹牛脚本

#### 页面架构

Vue3 + Vite2 + axios

#### 安装教程

1.  Node.js 下载地址https://nodejs.org/zh-cn/
2.  下载长期维护版无脑下一步

#### 使用说明

1.  项目目录打开 PowerShell
2.  npm install
3.  npm run dev

#### 参与贡献

1.  WebMan 妖火(18936)
1.  RemMai 妖火(43315)
