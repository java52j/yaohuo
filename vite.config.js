import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import styleImport from 'vite-plugin-style-import';
//import { join } from 'path'

//function resolve(dir){
//  return join(__dirname,dir)
//}
// https://vitejs.dev/config/
const path = require('path');
export default defineConfig({
  // 静态资源服务的文件夹
  publicDir: 'public',
  base: './',
  // 静态资源处理
  assetsInclude: '',
  plugins: [
    vue(),
    styleImport({
      libs: [
        {
          libraryName: 'vant',
          esModule: true,
          resolveStyle: (name) => `vant/es/${name}/style`,
        },
      ],
    }),
  ],
  resolve: {
    alias: {
      '@': path.resolve(__dirname, 'src'),
    },
    extensions: ['.js', '.vue', '.json'],
  },
  server: {
    host: '0.0.0.0',
    port: 9527,
    open: false,
    proxy: {
      '/api': {
        target: 'https://yaohuo.me',
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, '')
      }
    }
  },
  //打包配置
  build: {
    //指定输出路径
    outDir: 'dist',
    //生成静态资源的存放路径
    assetsDir: 'assets',
    //启用/禁用 CSS 代码拆分
    cssCodeSplit: true,
  },
});
